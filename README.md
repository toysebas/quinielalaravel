<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**
- **[Curotec](https://www.curotec.com/services/technologies/laravel/)**
- **[OP.GG](https://op.gg)**
- **[WebReinvent](https://webreinvent.com/?utm_source=laravel&utm_medium=github&utm_campaign=patreon-sponsors)**
- **[Lendio](https://lendio.com)**

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).


## Quiniela

Sistema para la gestion de la QUINIELA
Requerimientos

    PHP 7.1
    MySQL 5.6
    Configuración de la BD en las variables de entorno (o archivo .env)
    Composer instalado

Configuración

El proyecto cuenta con un archivo de configuración .env, en caso de no contar con el se toman como valores lo que este definido como variables de entorno para el contexto de la ejecución, las variables importantes son:

    APP_NAME Nombre de la aplicacion (Por default QuinielaLaravel)
    APP_ENV Entorno de ejecución, los valores posibles son:
        local
        testing
        production
    APP_DEBUG Es un valor true/false el cual indica si se mostraran mensajes de error detallados y si DebugBar estara activo
    APP_URL En esta variable se indica la URL en la cual se ejecutara el sistema
    DB_CONNECTION Indica el motor de BD (mysql, sqlite, etc)
    DB_HOST Indica el host de base de datos (Ej. 127.0.0.1)
    DB_PORT Indica el puerto de la base de datos (3306 para mysql, etc)
    DB_DATABASE Indica la base de datos en la cual se ejecutara el sistema (Debe estar ya creada)
    DB_USERNAME Indica el usuario de conexion a la BD
    DB_PASSWORD Indica la contraseña de conexion a la BD

Para facilitar la configuracion se proporciona el archivo .env.example el cual sirve de plantilla para crear el archivo de configuración .env
Instalación del proyecto

    Clonar el repositorio git clone https://toysebas@bitbucket.org/toysebas/quinielalaravel.git
    Cambiar el path al proyecto
    Instalar las dependencias composer install
    Crear el archivo de configuración cp .env.example .env
    Ejecutar las migraciones php artisan migrate
    Crear usuario para pruebas php artisan make:user
    Ejecutar el servidor de desarollo php artisan serve --host=0.0.0.0
